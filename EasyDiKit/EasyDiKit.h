//
//  EasyDiKit.h
//  EasyDiKit
//
//  Created by Evgeniy Abashkin on 15/02/2019.
//  Copyright © 2019 SMediaLink. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for EasyDiKit.
FOUNDATION_EXPORT double EasyDiKitVersionNumber;

//! Project version string for EasyDiKit.
FOUNDATION_EXPORT const unsigned char EasyDiKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <EasyDiKit/PublicHeader.h>


